import { useEffect, useRef, useState } from "react";
import { socket } from "../Utils/webSocket";
import socketIOClient from "socket.io-client";

let userStream;
let rtcPeerConnection;
let video;
let consumer;
let iceServers = {
  iceServers: [
    {
      "urls": [
        "stun:stun.l.google.com:19302",
        "stun:stun1.l.google.com:19302",
        "stun:stun2.l.google.com:19302",
        "stun:stun3.l.google.com:19302",
      ]
    }
  ]
}

function Producer() {

  console.log('Producer is Triggered');
  
  const onIceCandidateFunction = (event) => {

    console.log('[EVENT::ONICECANDIDATEFUNCTION::PRODUCER]::', event);

    if(event.candidate){
      console.log('[NAMES]', consumer);
      socket.emit("producer-icecandidate", event.candidate, consumer);
    }
  }

  const onTrackFunction = (event) => {
      video.srcObject = event.streams[0];
      video.onloadedmetadata = () => {
        video.play();
      }
  }

  socket.on('ice-candidate', (candidate, consumerName) => {
      // rtcPeerConnection = new RTCPeerConnection(iceServers);
      // rtcPeerConnection.onicecandidate = onIceCandidate;
      rtcPeerConnection.ontrack = onTrackFunction;
      rtcPeerConnection.addTrack(userStream.getTracks()[0], userStream);
      rtcPeerConnection.addTrack(userStream.getTracks()[1], userStream); 
      rtcPeerConnection
                    .createOffer()
                    .then((offer) => {
                      rtcPeerConnection.setLocalDescription(offer);
                      socket.emit('producer-offer', (offer, consumerName));
                    })
                    .catch((error) => {
                      console.log('Logging Errors');
                    })
      
      let icecandidate = new RTCIceCandidate(candidate);
      rtcPeerConnection.addIceCandidate(icecandidate);
  });

  socket.on("answer", (answer) => {
    rtcPeerConnection.setAnswer(answer);
  })

  useEffect(() => {
    let constraints = { audio: true, video: { width: 1920, height: 720 } };
        navigator.mediaDevices.getUserMedia = navigator.mediaDevices.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
    
        navigator.mediaDevices.getUserMedia(constraints)
          .then(function (mediaStream) {
            video = document.getElementById('user-video');
    
            userStream = mediaStream;
            video.srcObject = mediaStream;
            video.onloadedmetadata = function (e) {
              video.play();
            };

            rtcPeerConnection = new RTCPeerConnection(iceServers);
            rtcPeerConnection.onicecandidate = onIceCandidateFunction();
          })
          .catch(function (err) { console.log(err.name + ": " + err.message); });
    }, []);


  return <>
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <div className="container-fluid">
        <a className="navbar-brand">VideoChat</a>
      </div>
    </nav>
    <div className="video-chat-room">
      <video id="user-video" autoPlay />
    </div>
  </>
}

export default Producer;
