import { useEffect, useRef, useState } from "react";
import { socket } from "../Utils/webSocket";

let rtcPeerConnection
let iceServers = {
  iceServers: [
    { urls: "stun:stun.services.mozilla.com" },
    { urls: "stun:stun.l.google.com:19302" },
  ],
};

function Consumer() {

  let video = document.getElementById('user-video');
  console.log('Consumer is Triggered');

  const onIceCandidateFunction = (event) => {
    console.log('[EVENT::ONICECANDIDATEFUNCTION::CONSUMER]::', event);
    if (!event) {
      console.log('Inside if');
      return;
    }

    let consumerName = localStorage.getItem('hostName');
    let producerName = localStorage.getItem('producerName');

    console.log('[NAMES]', consumerName, producerName);
    socket.emit("consumer-icecandidate", event.candidate, consumerName, producerName);

  }

  socket.on('ice-candidate', (candidate) => {
    let icecandidate = new RTCIceCandidate(iceServers);
    rtcPeerConnection.addIceCandidate(icecandidate)
  })

  socket.on("offer", (offer, consumerName) => {

    rtcPeerConnection.setRemoteDescription(offer);
    rtcPeerConnection
      .createAnswer()
      .then((answer) => {
        rtcPeerConnection.setLocalDescription(answer);
        socket.emit("answer", answer, consumerName);
      })
      .catch((error) => {
        console.log(error);
      });
  })

  const onTrackFunction = (event) => {
    video.srcObject = event.streams[0];
    video.onloadedmetadata = (event) => {
      video.play();
    };
  }

  useEffect(() => {
    console.log('RTCPEERCONNCETION')
    rtcPeerConnection = new RTCPeerConnection(iceServers);
    rtcPeerConnection.onicecandidate = onIceCandidateFunction();
    rtcPeerConnection.ontrack = onTrackFunction;
  })

  return <>
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <div className="container-fluid">
        <a className="navbar-brand">VideoChat</a>
      </div>
    </nav>
    <div className="video-chat-room">
      <video id="user-video" autoPlay />
    </div>
  </>
}

export default Consumer;
