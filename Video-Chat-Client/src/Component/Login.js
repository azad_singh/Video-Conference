import { useState } from "react";
import { useHistory } from 'react-router-dom';
import ModalComponent from './ModalComponent'; 
import socketIOClient, { Socket } from "socket.io-client";
import { socket } from "../Utils/webSocket";
import './Login.css';

function Login(props) {

  const [name, setName] = useState('');
  const [hostName, setHostName] = useState('');
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);


  let history = useHistory();
  const onNameChangeHandler = (e) => {
    e.preventDefault();
    setHostName(e.target.value);
  }

  
  const onStartCallHandler = (e) => {
    console.log('I am in StartCallHandler')
    setHostName('');

    const socketID = socket.id;
    console.log('[SOCKET ID]::::', socketID);
    socket.emit("start", {hostName: hostName, socketID: socketID});
    
    localStorage.setItem('user', hostName);

    history.replace('/producer');
  }

  const onJoinCallHandler = (e) => {
    handleShow();
  }

  const onHostNameHandler = (e) => {
    setHostName(e.target.value);
  }

  const onNameHandler = (e) => {
    setName(e.target.value);
  }


  const onConnect = (e) => {
    console.log('Host Name   : ', hostName);
    console.log('Producer Name        : ', name);

    const socketID = socket.id;

    localStorage.setItem('hostName', hostName);
    localStorage.setItem('producer', name);

    socket.emit("join", { hostName: hostName, name: name, socketID: socketID});
    history.replace('/consumer');
  }

  return <>
    <ModalComponent show={show} handleClose ={handleClose} handleShow={handleShow} onHostNameHandler={onNameHandler} onConnect={onConnect} />    
    <div className="row">
      <div className="col-md-12 col align-self-center login-container">
        <div className="login">
          <h1 className="login__title">VIDEO CHAT </h1><br></br>
          <input type="email" className="form-control" id="exampleFormControlInput1" placeholder="Name" value={hostName} onChange={onNameChangeHandler} />
          <br></br>
          <div className="row g-2 align-items-center">
            <div className="col-md-6">
              <button className="btn btn-primary btn-sm" type="submit" onClick={onStartCallHandler}>START CALL</button>
            </div>
            <div className="col-md-6">
              <button className="btn btn-primary btn-sm" type="submit" onClick={onJoinCallHandler}>JOIN CALL</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </>
}

export default Login; 