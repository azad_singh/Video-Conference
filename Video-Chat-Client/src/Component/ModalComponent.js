import { Modal, Button } from 'react-bootstrap';

export default function ModalComponent(props) {
 
  return (
    <>
      <Modal show={props.show} onHide={props.handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Conference Room</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <input type="text" placeholder="Host Name" value={props.hostName} onChange={props.onHostNameHandler} />
        </Modal.Body>
        <Modal.Footer>
          <Button variant="primary" onClick={props.onConnect}>
            Connect
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}