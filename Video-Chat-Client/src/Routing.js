import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Login from "./Component/Login"; 
import Producer from "./Component/Producer";
import Consumer from "./Component/Consumer";

function Routing(){
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={Login} />
        <Route exact path="/producer" component={Producer} />
        <Route exact path="/consumer" component={Consumer} />
      </Switch>
    </Router>
  )
}

export default Routing;