class DB {
  constructor(){

    if(!DB.instance){
      this.userList = [];
      DB.instance = this;
    }
    return DB.instance;
  }

  addUser = (user) => {
      return this.userList.push(user);
  }

  listUsers = () => {
    return this.userList;
  }

  findUserByName = (name) => {
    console.log('[HOSTNAME] : ', name);    
    const nameResult =  this.userList.find(result => result.hostName == name);

    if(nameResult === undefined){
      return false;
    }

    return nameResult.socketID;
  }

  findUserByConsumerName = (name) => {
    console.log('[HOSTNAME] : ', name);    
    const nameResult =  this.userList.find(result => result.name == name);

    if(nameResult === undefined){
      return false;
    }

    return nameResult.socketID;
  }

  findProducerNameusingConsumerName = (name) => {
    const nameResult =  this.userList.find(result => result.name == name);

    if(nameResult === undefined){
      return false;
    }

    return nameResult.hostName;
  }
}

const DBConnection = new DB();
Object.freeze(DBConnection);

module.exports = {DBConnection};