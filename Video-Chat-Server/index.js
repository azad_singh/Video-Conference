const express = require("express");
const socket = require("socket.io");
const app = express();
const { DBConnection } = require('./lib/db');

//Starts the server
let server = app.listen(4000, function () {
  console.log("Server is running on Port 4000");
});

app.use(express.static("public"));

const db = DBConnection;

let io = socket(server, {
  cors: {
    origin: 'http://localhost:3000',
    method: ['GET', 'POST']
  }
});

//Triggered when a client is connected.
io.on("connection", function (socket) {

  //Trigger when user hits the start button
  socket.on("start", function (user){
      console.log("MESSAGE WHEN PRODUCER STARTED THE CALL ::: ", user);
      db.addUser(user);

      console.log('[PRODUCER::LISTUSERS::START]', db.listUsers());
      socket.join(user.socketID);
  });

  //Triggered when a peer hits the join room button.
  socket.on("join", function (user) {
     console.log("MESSAGE WHEN CONSUMER JOINED THE CALL ::: ", user);

     let receiverSocketID = db.findUserByName(user.name);
     console.log('[RECEIVER SOCKET ID]:::', receiverSocketID);

     socket.join(user.socketID);
    //  socket.emit("joined", {user: user, receiverSocketID: receiverSocketID});
  });


  //Triggered when server gets an icecandidate from a peer in the room.
  socket.on("consumer-icecandidate", (candidate, consumerName, producerName) => {
    
    console.log("[CANDIDATE::CONSUMER]");
    console.log(candidate);

    let id = db.findUserByName(producerName);
    io.to(id).emit("ice-candidate", candidate, consumerName); //Sends Candidate to the other peer in the room.
  });

  socket.on("producer-icecandidate", (candidate, consumerName) => {
     let id = db.findUserByConsumerName(consumerName);
     
     console.log("[CANDIDATE]:::", candidate);
     io.to(id).emit("ice-candidate", (candidate));
  })

  socket.on('producer-offer', (offer, consumerName) => {
    let id = db.findUserByConsumerName(consumerName);

    console.log('[PRODUCER-OFFER]::::', offer);
    io.to(id).emit("offer", (offer, consumerName));
  })

  socket.on("answer", (answer, consumerName) => {
    console.log('[ANSWER]:::', answer);
    let name = db.findProducerNameusingConsumerName(consumerName);
    let id = db.findUserByName(name);

    io.to(id).emit("answer", answer);
  })
 
});
