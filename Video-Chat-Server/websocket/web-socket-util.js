const wrtc = require('wrtc'); 
const { DBConnection } = require('../lib/db');

const db = DBConnection;

const pc_config = {
  iceServers: [
    {
      urls: "stun:stun.l.google.com:19302",
    },
  ],
};

const createRecevierPeerConnection = (io, hostName, senderSocketID) => {
  console.log('[HOSTNAME]:::', hostName, senderSocketID);
  const pc = new wrtc.RTCPeerConnection(pc_config);
  console.log('[ADDING CANDIDATE IN SERVER]')
  
  pc.onicecandidate = (e) => {
    console.log("[CreateRecevierPeerConnection]::OnIceCandidate", senderSocketID, e.candidate);
    io.to(senderSocketID).emit('getSenderCandidate', {candidate: e.candidate});
  }

  pc.ontrack = (e) => {
    console.log("[CreateRecevierPeerConnection]::Track", e);
    const hostResult = db.findUserByName(hostName);
    if(hostResult != undefined) {
      db.addUser({id: senderSocketID, stream: e, hostName: hostName});
      console.log('[Sending Stream to Client]', db.listUsers());
    } else {
      db.updateUser(senderSocketId, {stream: e}); 
      console.log('>>>>>>>>>>>>>>>>>>:: ', db.listUsers());    
    }
    // io.to(senderSocketId).emit('join-user', {id: senderSocketId, stream: e.stream, roomId: roomId});
    // Broadcast message that user has joined
  }

  return pc;
};



module.exports = {
  createRecevierPeerConnection
};